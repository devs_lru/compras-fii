﻿///$tab Main
$(Must_Include=..\05 qvw\qvs\presentación\main.qvs);
///$tab 02. Variables
$(Must_Include=..\05 qvw\qvs\presentación\02. variables.qvs);
///$tab Pruebas



///$tab 03. OC
$(Must_Include=..\05 qvw\qvs\presentación\03. oc.qvs);
///$tab 04. Nivel de Servicio
$(Must_Include=..\05 qvw\qvs\presentación\04. nivel de servicio.qvs);
///$tab 05. Ingreso Proveedor
$(Must_Include=..\05 qvw\qvs\presentación\05. ingreso proveedor.qvs);
///$tab 06. Variacion de costos
$(Must_Include=..\05 qvw\qvs\presentación\06. variacion de costos.qvs);
///$tab 07. Ppto
$(Must_Include=..\05 qvw\qvs\presentación\07. ppto.qvs);
///$tab 08. LinkTable
$(Must_Include=..\05 qvw\qvs\presentación\08. linktable.qvs);
///$tab 09. Ventas
$(Must_Include=..\05 qvw\qvs\presentación\09. ventas.qvs);
///$tab 10. Articulos
$(Must_Include=..\05 qvw\qvs\presentación\10. articulos.qvs);
///$tab 11. Calendario
$(Must_Include=..\05 qvw\qvs\presentación\11. calendario.qvs);
///$tab 12. Resumen Indicadores 
$(Must_Include=..\05 qvw\qvs\presentación\12. resumen indicadores.qvs);
///$tab 13. Ppto Ventas
$(Must_Include=..\05 qvw\qvs\presentación\13. ppto ventas.qvs);
///$tab 14. Indicadores Inventarios
$(Must_Include=..\05 qvw\qvs\presentación\14. indicadores inventarios.qvs);
///$tab Islas
$(Must_Include=..\05 qvw\qvs\presentación\islas.qvs);
///$tab Salida
$(Must_Include=..\05 qvw\qvs\presentación\salida.qvs);
