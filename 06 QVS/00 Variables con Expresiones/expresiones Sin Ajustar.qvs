﻿/* ==================================================================================================
	Variables de analisis de conjuntos (Set Analysis) para los peridos de tiempo 
================================================================================================== */

	/* --------------------------------------------------------------------------------------------------
	vSet_Calendario concatena todos los campos pertenecientes al calendario maestro para utilizarlos
	en el analisis de conjuntos de vSet_Analisis_Tiempo. Esto hace que no se tengan en cuenta 
	selecciones de tiempo para vitar distorición en las cifras al seleccionar diversos periodos de tiempo.
	-------------------------------------------------------------------------------------------------- */
	vSet_Calendario	[%Pk_Fecha]=,[Archivo]=,[Año]=,[Año_Quartil]=,[Año_SemanaRetail]=,[Año_Semestre]=,[Dia Semana]=,[Fecha]=,[Marcacion_FDS_Festivo]=,[Mes]=,[Mes_Num]=,[Numero Dia Semana Retail]=,[Periodo]=,[Quartil]=,[Semana Año]=,[Semana Mes]=,[Semana Retail]=,[Semestre]=,[Tipo Calendario]=,[Tipo Dia]=,[Tipo Dia Festivo]=

	/* --------------------------------------------------------------------------------------------------
	vSet_Analisis_Tiempo define los peridos de tiempo baso en los meses par YTD, MTD y MAT
	-------------------------------------------------------------------------------------------------- */
	vSet_Analisis_Tiempo	$(=Chr(39)&           	
		If('$1' ='YTD' , 
			if( GetSelectedcount(Mes) <= 1, 
				'$(vSet_Calendario),' & '[Mes_Num]={"<=$(=max([Mes_Num]))"}',
				'$(vSet_Calendario),' & '[Mes_Num]={">=$(=min([Mes_Num])) <=$(=max([Mes_Num]))"}'
			   ),    	
		
		If('$1' ='YTDC',
			'$(vSet_Calendario),' & 
			'[Fecha]={"<=$(=Num(AddYears( $(=max( {<%Pk_Origen={"Ventas"}, [Tipo Calendario]={'MTH'}>} [Fecha])) , $2) ) )"}',
		
		If('$1' ='MTD' ,'[Año]=,[Mes_Num]={"$(=max([Mes_Num]))"}'  ,
		If('$1' ='MTDC','[Año]=,[Mes_Num]={"$(=max([Mes_Num]))"}, Dia={"<=$(=max( {<%Pk_Origen={"Ventas"}, [Tipo Calendario]={"MTH"}>} [Dia]))"}' ,
		
		If(LEFT('$1',3) ='MTH' ,
		 	'$(vSet_Calendario),' 
		,      	  	
		
		If('$1' ='MAT' ,'$(vSet_Calendario),'&'Num_Periodo={">=$(=Num(AddMonths($(=Max(Num_Periodo)), -$(vD_Num_Periodo)))) <=$(=Max(Num_Periodo))"}' ,    	  	
		If('$1' ='MATC','$(vSet_Calendario),'&'Num_Periodo={">=$(=Num(AddMonths($(=Max(Num_Periodo)), -$(vD_Num_Periodo))))"} , [Fecha]={"<=$(=max( {<%Pk_Origen={"Ventas"}, [Tipo Calendario]={'MTH'}>} [Fecha]))"}' ,    	  	
		If(LEFT('$1',4) ='MATH','$(vSet_Calendario),'&'Num_Periodo={"$(=Num(AddMonths($(=Max(Num_Periodo)), -$(=Rangesum( Num(Right('$1',2),'0'))))))"}',''
		)
		
		)))))))
	&Chr(39) )

	/* --------------------------------------------------------------------------------------------------
		vSet_Año define el año al que se aplicara el periodo de meses definido en vSet_Analisis_Tiempo
	-------------------------------------------------------------------------------------------------- */
	vSet_Año	$(= Chr(39) & $(vSet_Analisis_Tiempo($1,$2)) & If( LEFT('$1',3)<>'MAT' , ',[Año]={$(=max([Año])+$2)}' , ',[Año]={$(=max([Año])+$2)}' ) &', [Tipo Calendario]={"MTH"}'& Chr(39) )

	/* --------------------------------------------------------------------------------------------------
		vSet_Mes Selecciona el mes maximo basado en la fecha
	-------------------------------------------------------------------------------------------------- */
	vSet_Mes  $(= Chr(39) & $(vSet_Analisis_Tiempo($1,$2)) & '%Pk_Fecha = {$(=floor(monthstart(addmonths(max(%Pk_Fecha),$2))))}'  &Chr(39) )



/* ==================================================================================================
	Variables de diseño 
================================================================================================== */

NumberFormat	 #.##0;( #.##0)
PorcentageFormat	 #.##0,0%;- #.##0,0%
vD_AnalisisBotonLetra	If( '$(vD_AnalisisCompras)'='$1',$(vD_ColorVerde) , $(vD_ColorNegro) )
vD_AnalisisCompras	1
vD_ColorGris	RGB(192,192,192)
vD_ColorNaranja	RGB(255,192,0)
vD_ColorNegro	RGB(0,0,0)
vD_ColorVerde	RGB(15,139,67)
vD_ConversionValor	only(%M_FactorConversionValor)
vD_FlechaRoja	qmem://<bundled>/BuiltIn/arrow_n_r.png
vD_FlechaVerde	qmem://<bundled>/BuiltIn/arrow_s_g.png
vD_LedGreen	qmem://<bundled>/BuiltIn/led_g.png
vD_LedOrange	qmem://<bundled>/BuiltIn/led_o.png
vD_LedRed	qmem://<bundled>/BuiltIn/led_r.png
vD_MostrarSA	0
vD_NumPeriodos	11
vD_PestañaFondo	If(GetActiveSheetId()='Document\SH$1',$(vD_ColorVerde),White())

/* ==================================================================================================
	Variables de diseño 
================================================================================================== */

vD_Pruebas	Num( Sum({< $(=$(vSet_Mes($1,$2))), [Id Indicador] = {$3} >} [Denominador Indicador]) , '$(NumberFormat)')  

vD_ReportesBotonLetra	If( '$(vD_ReportesCompras)'='$1',$(vD_ColorVerde) , $(vD_ColorNegro) )
vD_ReportesCompras	10
vD_RotacionTrend	1
vD_SProveedor	1
vD_ValorNulo	qmem://<bundled>/BuiltIn/minus.png
vD_Verde	RGB(15,139,67)
vEt_PeriodoTiempo	Date(Addyears(Max(Periodo), $1), 'MMM YYYY')

/* ==================================================================================================
	Variables de diseño 
================================================================================================== */

	/* --------------------------------------------------------------------------------------------------
		vExp_KPIEjecComp = Ejecucion compras, contiene el valor de la compra ejecutada
		Cuenta con tres parametros
		$1 = YTD - MTD - MAT
		$2 = numero entero positivo o negativo que demarca la cantidad de años atras o delante con los que se quiere comprar
			o actual
			-1 año anterior a la seleccion del año
			1 año posterior a la seleccion del año
		$3 = numero del indicador que presentaremos, se toma el numerador
	-------------------------------------------------------------------------------------------------- */
	vExp_KPIEjecComp	Num( Sum({< $(=$(vSet_Mes($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]),'$(NumberFormat)')



	/* --------------------------------------------------------------------------------------------------
		vExp_KPIEjecComp = Ejecucion compras, contiene el valor de la compra ejecutada
		Cuenta con tres parametros
		$1 = YTD - MTD - MAT
		$2 = numero entero positivo o negativo que demarca la cantidad de años atras o delante con los que se quiere comprar
			o actual
			-1 año anterior a la seleccion del año
			1 año posterior a la seleccion del año
		$3 = numero del indicador que presentaremos, se toma el numerador
	-------------------------------------------------------------------------------------------------- */
	vExp_KPIDashboard	Num( 
	Sum({< $(=$(vSet_Mes($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]) / 
	Sum({< $(=$(vSet_Mes($1,$2))), [Id Indicador] = {$3} >} [Denominador Indicador]) 
	, If(Index($(vExp_KPIDashboard_Nombre($3)),'%') > 0, '$(PorcentageFormat)', '$(NumberFormat)'))  

	vExp_KPIDashboard_Di	Num( 
	Sum({< $(=$(vSet_Mes($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]) / 
	Sum({< $(=$(vSet_Mes($1,$2))), [Id Indicador] = {$4} >} [Numerador Indicador]) 
	, If(Index($(vExp_KPIDashboard_Nombre($3)),'%') > 0, '$(PorcentageFormat)', '$(NumberFormat)'))  

	vExp_KPIDashboard_DiYTD	Num( 
	Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]) / 
	Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$4} >} [Numerador Indicador]) 
	, If(Index($(vExp_KPIDashboard_Nombre($3)),'%') > 0, '$(PorcentageFormat)', '$(NumberFormat)'))  

	vExp_KPIDashboard_Nombre	Concat({< [Id Indicador] = {$1} >} DISTINCT Indicador,'')





vExp_AdherenciaCompras	Num( (Sum({< $(=$(vSet_Año($1,$2))) >} [Cantidad Recibida a Tiempo]) / Sum({< $(=$(vSet_Año($1,$2))) >} [Total requerido]) ), '$(NumberFormat)') 

vExp_AdherenciaConsumo	Num( $(vExp_ServicioProveedor($1,$2,$3,$4,0,$6,$7,0,0,0,0,$12)) / $(vExp_ServicioProveedor($1,$2,$3,0,$5,$6,$7,$8,$9,0,0,$12)), '$(PorcentageFormat)')

vExp_AdherenciaFarma	Num( $(vExp_ServicioProveedor($1,$2,$3,0,$5,$6,$7,0,0,0,0,$12)) / $(vExp_ServicioProveedor($1,$2,$3,0,$5,$6,$7,$8,$9,0,0,$12)), '$(PorcentageFormat)')

vExp_AdherenciaTotal	Num( $(vExp_ServicioProveedor($1,$2,$3,$4,$5,$6,$7,0,0,0,0,$10)) / $(vExp_ServicioProveedor($1,$2,$3,$4,$5,$6,$7,$8,$9,0,0,$10)), '$(PorcentageFormat)')

vExp_Compras	sum( {< $(=$(vSet_Año($1,$2)))>} [Valor Compra] )

vExp_ConceptoIngPpto	Num( (sum( {< $(=$(vSet_Año($1,$2))), [$3] = {'$4','$5','$6'}, [$7] = {'$8'} >} [$9] )  /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_ConceptoIngreso%	Num( $(vExp_ConceptoIngValor($1,$2,$3,$4,$5,$6,$7))/$(vExp_ConceptoIngPpto($1,$2,$3,$4,$5,$6,$8,$9,$10)) , '$(PorcentageFormat)')

vExp_ConceptoIngValor	Num( (sum( {< $(=$(vSet_Año($1,$2))), [$3] = {'$4','$5','$6'} >} [$7] )  /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_ConsumoExcNovedad	Num( $(vExp_ServicioProveedor($1,$2,$3,$4,0,$6,$7,0,0,$10,$11,$12)) / $(vExp_ServicioProveedor($1,$2,$3,$4,0,$6,$7,$8,$9,$10,$11,$12)), '$(PorcentageFormat)')

vExp_DevolucionesCompras	Num( (sum( {< $(=$(vSet_Año($1,$2))), [$3] = {'$4'} >} [$5] )  /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_DevolucionesCompras%	Num( $(vExp_DevolucionesCompras($1,$2,$3,$4,$5))/$(vExp_DevolucionesCompras($1,$2,$6,$7,$8)) ,'$(PorcentageFormat)')

vExp_Ejecucion	Num( (sum({< $(=$(vSet_Año($1,$2))) >} [$3])/$(vD_ConversionValor) ), '$(NumberFormat)')  

vExp_EjecucionIngPro	Num( (sum({< $(=$(vSet_Año($1,$2))),[Concepto Ingreso] =- {'$3'} >} [Valor Dcto/Devolucion])/$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_EjecucionIngPro%	Num( $(vExp_EjecucionIngPro($1,$2,$3)) / $(vExp_Presupuesto($1,$2,$4)) ,'$(PorcentageFormat)')

vExp_ExcluendoNovedad	Num( $(vExp_ServicioProveedor($1,$2,$3,$4,$5,$6,$7,0,0,$10,$11,$12)) / $(vExp_ServicioProveedor($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)), '$(PorcentageFormat)')

vExp_Gr_KPIDashboard	Num(  
	Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]) / 
	Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3} >} ([Denominador Indicador])) 
, If(Index($(vExp_KPIDashboard_Nombre($3)),'%') > 0, '$(PorcentageFormat)', '$(NumberFormat)'))

vExp_Indicadores	Num(   
Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]) /   
Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3} >} [Denominador Indicador])   
, If(Index($(vExp_IndicadoresName($3)),'%') > 0, '$(PorcentageFormat)', '$(NumberFormat)'))

vExp_IndicadoresName	Concat({1< [Id Indicador] = {$1} >} DISTINCT Indicador,'')

vExp_IngProvFinancieros	Num( (sum({<$(=$(vSet_Año($1,$2))), [$3] =- {'$4'}, [$3] =- {'$5'}, [$3] = {'$6'} >} [$7]) /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_IngProvInversion	Num( (sum({<$(=$(vSet_Año($1,$2))), [$3] =- {'$4'},[$3] ={'$5'} >}[$6]) /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_IngProvOtros	Num( (sum({<$(=$(vSet_Año($1,$2))), [$3] = {'$4'} >}[$5]) /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_KPIEjec	Num( 
Sum({< $(=$(vSet_Mes($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]),'$(NumberFormat)')

vExp_KPIEjecCompYTD	Num( 
Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3} >} [Numerador Indicador]),'$(NumberFormat)')

vExp_KPIEjecYTD	Num( 
Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3}>} [Numerador Indicador]),'$(NumberFormat)')

vExp_KPIPptoYTD	Num( Sum({< $(=$(vSet_Año($1,$2))), [Id Indicador] = {$3} >} [Denominador Indicador]),'$(NumberFormat)')

vExp_MaxPeriodo	Max({1} Periodo,$1)

vExp_Presupuesto	Num( (sum({< $(=$(vSet_Año($1,$2))), [Tipo Ppto] = {'$3'}>}[Valor Ppto])/$(vD_ConversionValor) ), '$(NumberFormat)')  

vExp_ServicioProveedor	Num( (Count({<$(=$(vSet_Año($1,$2))), [$3] = {'$4','$5'}, $6 = {'$7','$8','$9'}, $10 = {'$11'} >} $12 ) / $(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_VariacionAbastacimiento	Num( (sum( {< $(=$(vSet_Año($1,$2))), [$3] =- {'$4'} >} [$5] )  /$(vD_ConversionValor) ), '$(NumberFormat)')

vExp_VariacionCostoFarma	Num( (sum({<$(=$(vSet_Año($1,$2))), [$3] = {'$4','$5'}, $6 ={'$7'}>} [$8]) /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_VariacionPpto	Num( (sum( {< $(=$(vSet_Año($1,$2))), [$3] = {'$4'}, $5 = {'$6'} >} [$7] )  /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_VariacionPptoCompras	Num( (sum( {< $(=$(vSet_Año($1,$2))), [$3] = {'$4'} >} [$5] )  /$(vD_ConversionValor) ), '$(NumberFormat)')   

vExp_VariacionPrecios	Num( (sum( {< $(=$(vSet_Año($1,$2))), [$3] = {'$4','$5'} >} [$6] )  /$(vD_ConversionValor) ), '$(NumberFormat)') 

vExp_VariacionPrecios%	Num( $(vExp_VariacionPrecios($1,$2,$3,$4,$5,$6))/$(vExp_VariacionPrecios($1,$2,$3,$4,$5,$7)) ,'$(PorcentageFormat)')

vExpFarmaExclNovedad	Num( $(vExp_ServicioProveedor($1,$2,$3,$4,0,$6,$7,0,0,$10,$11,$12)) / $(vExp_ServicioProveedor($1,$2,$3,$4,0,$6,$7,$8,$9,$10,$11,$12)), '$(PorcentageFormat)')

vExp_Total_Costo num(Sum({<$(=$(vSet_Año($1,$2)))>}[$3])/$(vD_ConversionValor),'$(NumberFormat)')

vExp_Cantidades num(Sum({<$(=$(vSet_Año($1,$2)))>}[$3]),'$(NumberFormat)')


/* ==================================================================================================
	Variables de ruta o direccionamiento 
================================================================================================== */

vFin	16/02/2017  4:31:27 PM
vR_Imagenes	..\01 QPM\Imagenes\
vR_Qvd	..\04 Qvd\
vR_QvdVentas	..\..\02 Ventas\04 Qvd\
