﻿Trace =====================================================================
6.1. Variación de Costos
=====================================================================;
/*	Fecha: 								21/05/2018
	Desarrollador: 						Miguel Angel Lemos
	Revision/Aprobacion: 				Miguel Angel Lemos
	Version: 							1
	Nombre Archivos QVD Origen:			D_01_Qvd_OrdenCompra_Tra
	Nombre Archivos QVD Resultante:		D_01_Qvd_VariacionCostos_Tra
	Descripcion:						Uno de los productos en el año actual 
										vs el mes de diciembre del año anterior,
										comparándolo de la misma manera con el 
										mes en curso del año actual y del mismo mes 
										del año anterior.
	Campo union modelo: 				%Pk_Costos_Link
	Regla de negocio: */

D_Variacion_Costos_Tmp0:
	LOAD
		[Articulo Cod],
		num(AddMonths(YearStart(Fecha),-1))
			&'_'& [Articulo Cod] 
			&'_'& [Unidad De Medida UDM]
			&'_'& [Proveedor NIT] 									as "Fecha Union Tmp",
		[Mundo Producto],
		ApplyMap('Map_MundoProducto', "Mundo Producto", 0)			as "Mundo Producto Id",
		[Fecha],
		[Tipo de Orden],
		//[Sucursal Id],
		[Ubicacion Proveedor Id],
		[Unidad De Medida UDM],
		[Proveedor NIT],
		[Proveedor Nombre],
		[Cantidad Recibida],
		[Valor Compra] 												as [Valor Costo Comp]
	From $(vR_Qvd)D_01_Qvd_OrdenCompra_Tra.qvd (QVD)
	Where year([Fecha]) <= year(Today());
	
/*--------------------------------------------------------------------------------------------*/
D_01_Qvd_Variacion_Costos_Tmp1:
	NoConcatenate
	LOAD
		[Articulo Cod],
		[Mundo Producto],
		"Mundo Producto Id",
		[Proveedor NIT],
		[Proveedor Nombre],
		//[Sucursal Id],
		[Unidad De Medida UDM],
		"Fecha",		
		"Fecha Union Tmp",
		Sum([Cantidad Recibida])	as [Cantidad Recibida],
		Sum([Valor Costo Comp]) 	as [Valor Costo Comp],
		Ceil(Sum(Rangesum([Valor Costo Comp])) / Sum(Rangesum([Cantidad Recibida])) ) as "Valor Costo Promedio"
	Resident D_Variacion_Costos_Tmp0
	Group By 
		[Articulo Cod],
		[Fecha Union Tmp],
		[Mundo Producto],
		"Mundo Producto Id",
		[Proveedor NIT],
		[Proveedor Nombre],
		//[Sucursal Id],
		[Unidad De Medida UDM],
		"Fecha";
	drop table D_Variacion_Costos_Tmp0;


/*--------------------------------------------------------------------------------------------*/
NoConcatenate
D_01_Qvd_Variacion_Costos_Tmp2:
	LOAD
		[Articulo Cod],
		[Mundo Producto],
		"Mundo Producto Id",
		[Proveedor NIT],
		[Proveedor Nombre],
		//[Sucursal Id],
		[Unidad De Medida UDM],
		Date("Fecha") as "Fecha",
		"Fecha Union Tmp",
		num(Fecha)&'_'&[Articulo Cod]&'_'&[Unidad De Medida UDM]&'_'&[Proveedor NIT] as Key2,
		[Cantidad Recibida],
		Num([Valor Costo Comp]) 	as [Valor Costo Comp],
		Num("Valor Costo Promedio")	as "Valor Costo Promedio"
	Resident D_01_Qvd_Variacion_Costos_Tmp1;
	DROP Table D_01_Qvd_Variacion_Costos_Tmp1;
	
	Left Join(D_01_Qvd_Variacion_Costos_Tmp2)
	LOAD
		num(monthstart(Fecha)) &'_'& [Articulo Cod] &'_'& [Unidad De Medida UDM] &'_'& [Proveedor NIT] as [Fecha Union Tmp],
		"Valor Costo Promedio" as [Valor Costo Prom DicAñoAnt]
	Resident D_01_Qvd_Variacion_Costos_Tmp2
	Where Month(Fecha) = 12;

	Left Join(D_01_Qvd_Variacion_Costos_Tmp2)
	LOAD
		num(AddYears("Fecha",1))&'_'&[Articulo Cod]&'_'&[Unidad De Medida UDM]&'_'&[Proveedor NIT] as Key2,
		"Valor Costo Promedio"	as [Valor Costo Prom MesActAñoAnt]		
	Resident D_01_Qvd_Variacion_Costos_Tmp2;

/*--------------------------------------------------------------------------------------------*/
NoConcatenate
	D_01_Qvd_VariacionCostos_Tra:
		LOAD *,
			if([Valor Costo Comp]=0 
				or [Valor Costo DicAñoAnt]=0,0,[Valor Costo Comp]) as [Valor Costo a Comp];
		LOAD 
			*,
			[Articulo Cod] 
				// &'_'& [Sucursal Id] 
				&'_'& [Proveedor NIT] 
				&'_'& Num(Fecha) 
				&'_'& [Mundo Producto Id] 
				&'_'& "Flag Producto Regulado" 
				&'_'& [Unidad De Medida UDM] as %Pk_Costos_Link, 
			[Cantidad Recibida]*[Valor Costo Prom DicAñoAnt]	as [Valor Costo DicAñoAnt],
			[Cantidad Recibida]*[Valor Costo Prom MesActAñoAnt]	as [Valor Costo MesActAñoAnt];
		LOAD
			[Articulo Cod],
			[Mundo Producto],
			"Mundo Producto Id",
			[Proveedor NIT],
			[Proveedor Nombre],
			//[Sucursal Id],
			[Unidad De Medida UDM],
			"Fecha",			
			"Fecha Union Tmp",
			applymap('Map_ArticuloRegulado',[Articulo Cod],'0') 		as "Flag Producto Regulado",
			applymap('Map_CanalClinicas',[Articulo Cod],'0') 			as "Flag Canal Clinicas",
			applymap('Map_CanalInstitucional',[Articulo Cod],'0') 		as "Flag Canal Institucional",
			applymap('Map_CanalCancerologico',[Articulo Cod],'0') 		as "Flag Canal Cancerologico",
			applymap('Map_CanalRetail',[Articulo Cod],'0') 				as "Flag Canal Retail",
			applymap('Map_CanalRetail',[Articulo Cod],'0') 				as "Flag Canal Otc",
			applymap('Map_CanalEpsPos',[Articulo Cod],'0') 				as "Flag Canal Pos",
			Rangesum([Cantidad Recibida])								as [Cantidad Recibida],
			Rangesum([Valor Costo Comp])								as [Valor Costo Comp],
			Rangesum([Valor Costo Promedio])							as [Valor Costo Promedio],
			Rangesum([Valor Costo Prom DicAñoAnt])						as [Valor Costo Prom DicAñoAnt],
			Rangesum([Valor Costo Prom MesActAñoAnt])					as [Valor Costo Prom MesActAñoAnt]			
		Resident D_01_Qvd_Variacion_Costos_Tmp2;
		Drop Table D_01_Qvd_Variacion_Costos_Tmp2;

	Store D_01_Qvd_VariacionCostos_Tra into $(vR_Qvd)D_01_Qvd_VariacionCostos_Tra.Qvd (Qvd);
	Drop table D_01_Qvd_VariacionCostos_Tra;